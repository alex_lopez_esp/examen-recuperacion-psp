package Ejercicio1;

import java.util.concurrent.ThreadLocalRandom;

public class Caza implements Runnable{
    private static final int KM=20000;
    String nombre;
    int distancia;
    int cazasMatados;

    public Caza(String nombre) {
        this.nombre = nombre;

    }

    @Override
    public void run() {
        for (int i = 0; i <KM; i+=500) {
            try {

                if (!(posibilidadDeMatarCaza()>80)){
                    calcularCaza();
                    System.out.println(nombre+" ha recorrido : "+(i)+" ha derribado 1 caza");
                }else {
                    System.out.println(nombre+" ha recorrido : "+(i)+" no  ha derribado cazas");

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        System.out.println(nombre+" Ha llegado a la estrella de la muerte "+cazasMatados);

    }
    public int posibilidadDeMatarCaza(){
        return ThreadLocalRandom.current().nextInt(0,100);

    }
    public synchronized void calcularCaza(){
        cazasMatados++;
    }

}
