package Ejercicio1;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio1 {
public static List<Thread> cazas=new ArrayList();
private static final int NUM_CAZAS=10;
    public static void main(String[] args) {
        try {

            for (int i = 0; i < NUM_CAZAS; i++) {
                cazas.add(new Thread(new Caza("Caza Nº"+(i+1))));
                cazas.get(i).start();
            }
            for (int i = 0; i < cazas.size(); i++) {
                cazas.get(i).join();

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
