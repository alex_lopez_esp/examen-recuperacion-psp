package Ejercicio2;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio2 {
public static List<Thread> cazas=new ArrayList();
private static final int NUM_CAZAS=10;
    public static void main(String[] args) {
        try {

            for (int i = 0; i < NUM_CAZAS; i++) {
                cazas.add(new Thread(new Caza("Caza Nº"+(i+1))));
                cazas.get(i).start();
            }

            for (int x = 0; x < cazas.size(); x++) {
                cazas.get(x).join();

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Total de cazas Matados: "+Escuadron.CAZAS);
        System.out.println("Se ha Resuelto");


    }

}
