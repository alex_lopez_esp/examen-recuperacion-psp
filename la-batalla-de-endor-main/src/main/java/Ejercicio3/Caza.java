package Ejercicio3;

import java.util.concurrent.ThreadLocalRandom;

public class Caza implements Runnable{

    private static final int KM=20000;
    String nombre;
    int distancia;
    int cazasMatados;

    public Caza(String nombre) {
        this.nombre = nombre;

    }

    @Override
    public void run() {
        try {
        for (int i = 0; i <KM; i+=500) {


            if (!(posibilidadDeMatarCaza() > 80)) {
                calcularCaza();
                System.out.println(nombre + " ha recorrido : " + (i) + " ha derribado 1 caza");
            } else {
                System.out.println(nombre + " ha recorrido : " + (i) + " no  ha derribado cazas");

            }
        }
            } catch (Exception e) {
                System.err.println(nombre+" ha muerto");
            }

        System.out.println(nombre+" Ha llegado a la estrella de la muerte "+cazasMatados);
        System.out.println("Ha llegado el caza "+nombre);
        System.exit(0);


    }
    public int posibilidadDeMatarCaza(){
        return ThreadLocalRandom.current().nextInt(0,100);

    }
    public synchronized void calcularCaza(){
        cazasMatados++;
        Escuadron.sumarTodosLosCazas();

    }

}
