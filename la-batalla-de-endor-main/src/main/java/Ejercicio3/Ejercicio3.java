package Ejercicio3;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio3 {
public static List<Thread> cazas=new ArrayList();
private static final int NUM_CAZAS=10;
    public static void main(String[] args) {
        try {

            for (int i = 0; i < NUM_CAZAS; i++) {
                cazas.add(new Thread(new Caza("Caza Nº"+(i+1))));
                cazas.get(i).start();
            }
            Thread estrellaDelaMuerte=new Thread(new EstrellaDeLaMuerte("Estrella de la muerte"));
            estrellaDelaMuerte.start();
            for (Thread caza : cazas) {
                caza.join();

            }
            estrellaDelaMuerte.join();


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Total de cazas Matados: "+ Escuadron.CAZAS);
        System.out.println("Se ha Resuelto");


    }

}
