package Ejercicio3;

import java.util.concurrent.ThreadLocalRandom;

public class EstrellaDeLaMuerte implements Runnable{
    String  nombre;

    public EstrellaDeLaMuerte(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void run() {
       try {
           Thread.sleep(2000);
           if (!(posibilidadMatarCaza()>70)){
               if (Ejercicio3.cazas.size()!=0) {
               Thread caza_matado=Ejercicio3.cazas.get(cazaMatado());
               while (!caza_matado.isAlive()){
                   caza_matado=Ejercicio3.cazas.get(cazaMatado());
               }
               caza_matado.interrupt();
                   System.out.println("El caza"+caza_matado.getName()+" Ha sido matado");
               }
               }else {
                   System.out.println("La rebelión ha fracasado");
                   System.exit(0);
               }


       } catch (Exception e) {
           e.printStackTrace();
       }

    }

    private int cazaMatado() {
        return ThreadLocalRandom.current().nextInt(0,10);
    }

    private int posibilidadMatarCaza() {
        return ThreadLocalRandom.current().nextInt(0,100);
    }
}
