package Ejercicio4;

import java.util.concurrent.ThreadLocalRandom;

public class CazasImperiales implements Runnable{
    String nombre;

    public CazasImperiales(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void run() {
        try {


            Thread.sleep(1000);
            if (!((posibilidadDeMatar() > 10))) {
                Thread soldadomatad = Ejercicio4.cazas.get(matarCaza());
                while (!soldadomatad.isAlive()) {
                    soldadomatad = Ejercicio4.cazas.get(matarCaza());
                }
                soldadomatad.interrupt();
                Ejercicio4.cazas.remove(soldadomatad);

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    public int posibilidadDeMatar(){
         return ThreadLocalRandom.current().nextInt(0,100);
    }
    public int matarCaza(){
        return ThreadLocalRandom.current().nextInt(0,10);
    }
}
