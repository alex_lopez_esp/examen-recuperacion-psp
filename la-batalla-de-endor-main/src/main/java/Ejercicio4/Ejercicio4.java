package Ejercicio4;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio4 {
public static List<Thread> cazas=new ArrayList();
    public static List<Thread>cazasImperiales=new ArrayList<>();
private static final int NUM_CAZAS=10;
private static final int NUM_CAZAS_IMPERIALES=5;
    public static void main(String[] args) {
        try {
            Thread estrellaDelaMuerte=new Thread(new EstrellaDeLaMuerte("Estrella de la muerte"));


            for (int i = 0; i < NUM_CAZAS; i++) {
                cazas.add(new Thread(new Caza("Caza Nº"+(i+1)),"Caza Nº"+(i+1)));
                cazas.get(i).start();
            }
            estrellaDelaMuerte.start();
            estrellaDelaMuerte.join();
            for (int i = 0; i < NUM_CAZAS_IMPERIALES; i++) {
                cazasImperiales.add(new Thread(new CazasImperiales("Caza Imperial Nº"+(i+1))));
                cazasImperiales.get(i).start();

            }


            for (Thread caza : cazas) {
                caza.join();
            }

            for (Thread cazasImperiales : cazasImperiales) {
                cazasImperiales.join();
            }



        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Total de cazas Matados: "+ Escuadron.CAZAS);
        System.out.println("Se ha Resuelto");
        System.exit(0);

    }

}
