package Ejercicio4;

import java.util.concurrent.ThreadLocalRandom;

public class EstrellaDeLaMuerte implements Runnable{
    String nombre;

    public EstrellaDeLaMuerte(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void run() {
       try {
           while (true) {
               Thread.sleep(2000);
               if (!(posibilidadMatarCaza() >= 70)) {
                   if (Ejercicio4.cazas.size() != 0) {
                       Thread caza_matado = Ejercicio4.cazas.get(cazaMatado());
                       while (!caza_matado.isAlive()) {
                           caza_matado = Ejercicio4.cazas.get(cazaMatado());
                       }
                       caza_matado.interrupt();
                       System.err.println("El caza" + caza_matado.getName() + " Ha sido matado Por La estrella de la muerte");
                   } else {
                       System.err.println("La rebelión ha fracasado");
                   }
               }



           }
        } catch(Exception e){
        e.printStackTrace();
    }

    }

    private int cazaMatado() {
        return ThreadLocalRandom.current().nextInt(0,10);
    }

    private int posibilidadMatarCaza() {
        return ThreadLocalRandom.current().nextInt(0,100);
    }
}
